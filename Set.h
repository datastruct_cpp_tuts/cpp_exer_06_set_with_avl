#pragma once

/* 
  Your task here is to define a set of integers. Since this is not a template class 
  you must declare your functions in the header file and define them in .cpp file.
*/

#include <cstddef>

struct Node {
  int value;
  std::size_t level;
  Node *left;
  Node *right;
};

class AVLTree {
private:
  size_t nodeCount = 0;

  Node *top = nullptr;

  bool exist(Node *top, int value);
  void inorder(Node *top, void *(f)(int));
  int getBalance(Node *node);
  int getLevel(Node *node);
  Node *leftRotate(Node *pivote);
  Node *rightRotate(Node *pivote);
  Node *insert(Node *node, int value);
  Node *remove(Node *root, int value, bool &removed);
  Node *minValueNode(Node *node);

public:
  void insert(int value);
  void remove(int value);
  bool exist(int value);
  void inorder(void *(f)(int));
  bool empty();
  size_t size();
};

class Set
{
private:
  AVLTree tree;

public:
  bool empty();
  size_t size();

};
