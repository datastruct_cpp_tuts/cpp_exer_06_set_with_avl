#include "Set.h"
#include <algorithm>
#include <cstddef>

void AVLTree::inorder(Node *top, void *(f)(int)) {
  if (top == nullptr) {
    return;
  }
  inorder(top->left, f);
  f(top->value);
  inorder(top->right, f);
}

void AVLTree::inorder(void *(f)(int)) { inorder(top, f); }

bool AVLTree::exist(Node *top, int value) {
  if (top == nullptr) {
    return false;
  }
  if (exist(top->left, value)) {
    return true;
  }
  if (top->value == value) {
    return true;
  }
  if (exist(top->right, value)) {
    return true;
  }
  return false;
}

bool AVLTree::exist(int value) { return exist(top, value); }

int AVLTree::getBalance(Node *node) {
  if (node == nullptr) {
    return 0;
  }
  return getLevel(node->left) - getLevel(node->right);
}

int AVLTree::getLevel(Node *node) {
  if (node == nullptr) {
    return 0;
  }
  return node->level;
}

Node *AVLTree::rightRotate(Node *pivote) {
  Node *leftChildPivote = pivote->left;
  Node *orphan = leftChildPivote->right;

  leftChildPivote->right = pivote;
  pivote->left = orphan;

  pivote->level = std::max(getLevel(pivote->left), getLevel(pivote->right)) + 1;
  leftChildPivote->level = std::max(getLevel(leftChildPivote->left),
                                    getLevel(leftChildPivote->right)) +
                           1;

  return leftChildPivote;
}

Node *AVLTree::leftRotate(Node *pivote) {
  Node *rightChildPivote = pivote->left;
  Node *orphan = rightChildPivote->left;

  rightChildPivote->left = pivote;
  pivote->right = orphan;

  pivote->level = std::max(getLevel(pivote->left), getLevel(pivote->right)) + 1;
  rightChildPivote->level = std::max(getLevel(rightChildPivote->left),
                                     getLevel(rightChildPivote->right)) +
                            1;

  return rightChildPivote;
}

void AVLTree::insert(int value) {
  ++nodeCount;
  top = insert(top, value);
}

Node *AVLTree::insert(Node *node, int value) {
  if (node == nullptr) {
    return new Node{value, 1};
  }

  // 1. insert
  if (value < node->value) {
    node->left = insert(node->left, value);
  } else if (value > node->value) {
    node->right = insert(node->right, value);
  } else {
    // Duplicates not allowed
    return node;
  }

  // 2. update level
  node->level = std::max(getLevel(node->left), getLevel(node->right)) + 1;

  // 3. Get balance factor
  int balance = getBalance(node);

  // 4. rotations

  // left left
  if (balance > 1) {
    if (value < node->left->value) {
      return leftRotate(node);
    }

    // right right
    if (value > node->right->value) {
      return rightRotate(node);
    }

    // left right
    if (value > node->left->value) {
      node->left = leftRotate(node->left);
      return rightRotate(node);
    }

    // right left
    if (value < node->right->value) {
      node->right = rightRotate(node->right);
      return leftRotate(node);
    }
  }

  return node;
}

void AVLTree::remove(int value) {
  bool removed = false;
  Node *root = remove(top, value, removed);
  if (removed) {
    --nodeCount;
  }
}

Node *AVLTree::minValueNode(Node *node) {
  Node *current = node;

  while (current->left != nullptr) {
    current = current->left;
  }

  return current;
}

Node *AVLTree::remove(Node *root, int value, bool &removed) {
  // 1. normal delete
  if (root == nullptr) {
    removed = true;
    return root;
  }

  if (value < root->value) {
    root->left = remove(root->left, value, removed);
  } else if (value > root->value) {
    root->right = remove(root->right, value, removed);
  } else {
    // found a node to be deleted

    // one child or no child
    if ((root->left == nullptr) || (root->right == nullptr)) {
      Node *tmp = root->left ? root->left : root->right;

      if (tmp == nullptr) {
        // no child
        // NOTE tmp is leaf, delete "it"
        tmp = root;
        root = nullptr;
      } else {
        // one child
        // NOTE tmp is child, delete "it"
        *root = *tmp;
      }

      delete tmp;
    } else {
      // two child
      // get inorder successor (smallest in the rigth tree)
      Node *tmp = minValueNode(root->right);

      // copy successor's data to this node
      root->value = tmp->value;

      // delete successor
      root->right = remove(root->right, tmp->value, removed);
    }
  }

  if (root == nullptr) {
    return root;
  }

  // 2. update level
  root->level = std::max(getLevel(root->right), getLevel(root->left)) + 1;

  // 3. get balance factor
  int balance = getBalance(root);

  if (balance > 1) {
    // left left
    if (getBalance(root->left) >= 0) {
      return rightRotate(root);
    }

    // left right
    if (getBalance(root->left) < 0) {
      root->left = leftRotate(root->left);
      return rightRotate(root);
    }

    // right right
    if (getBalance(root->right) <= 0) {
      return leftRotate(root);
    }

    // right left
    if (getBalance(root->right) > 0) {
      root->right = rightRotate(root->right);
      return leftRotate(root);
    }
  }

  return root;
}

bool AVLTree::empty() { return top == nullptr; }

size_t AVLTree::size() { return nodeCount; }

/////////////////////////////////////////////////
///////// SET
/////////////////////////////////////////////////

bool Set::empty() {
  return tree.empty();
}

size_t Set::size() {
  return tree.size();
}
