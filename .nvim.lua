vim.cmd("set expandtab")

vim.api.nvim_create_augroup("user_startup_local", { clear = true })
vim.api.nvim_create_autocmd("User", {
	group = "user_startup_local",
	pattern = "VeryLazy",
	callback = function()
		vim.cmd("Lazy load overseer.nvim")
		local overseer = require("overseer")
		overseer.register_template({
			name = "Clean code",
			builder = function(params)
				return {
					cmd = { "bash" },
					args = { "-c", "rm", "*.h.gch", "||", "true" },
				}
			end,
		})
	end,
})
